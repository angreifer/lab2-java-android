package com.example.language;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void onButtonClick1(View V){
        Intent intent = new Intent(MainActivity.this, AlphabetActivity.class);
        startActivity(intent);
    }

    public void onButtonClick2(View V){
        Intent intent = new Intent(MainActivity.this, VocabularyActivity2.class);
        startActivity(intent);
    }

    public void onButtonClick3(View V){
        Intent intent = new Intent(MainActivity.this, TranslatorActivity.class);
        startActivity(intent);
    }

    public void onButtonClick4(View V){
        Intent intent = new Intent(MainActivity.this, MapActivity.class);
        startActivity(intent);
    }


}