package com.example.language;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class TranslatorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translator);
    }

    public void onButtonClick(View V){
        Intent intent = new Intent(TranslatorActivity.this, MainActivity.class);
        startActivity(intent);
    }
}